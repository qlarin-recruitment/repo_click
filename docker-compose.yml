version: "3.9"

services:

  app: # in production requires additional targets to build and move only useful sources without mounting codebase
    build:
      context: docker/php
      dockerfile: Dockerfile
    container_name: app
    restart: unless-stopped
    volumes:
      - ${APP_DIR}:/var/www/html:delegated
      - ${FILESTORAGE_DIR}:/data:delegated
    working_dir: /var/www/html
    environment:
      APP_ENV: dev
      APP_DEBUG: 0
      XDEBUG_CLIENT_HOST: host.docker.internal
      SOURCE_IMAGE_DIR: /data/images
      BUCKET_CREDENTIALS_KEY: ${BUCKET_CREDENTIALS_KEY}
      BUCKET_CREDENTIALS_SECRET: ${BUCKET_CREDENTIALS_SECRET}
      BUCKET_REGION: ${BUCKET_REGION}
      BUCKET_SERVER_ENDPOINT: http://minio:9000
  
  app-cli: # in production requires another slim target for only command operations
    build:
      context: docker/php
      dockerfile: Dockerfile
    container_name: app-cli
    volumes:
      - ${APP_DIR}:/var/www/html:delegated
      - ${FILESTORAGE_DIR}:/data:delegated
    working_dir: /var/www/html
    entrypoint: ["php", "bin/console"]
    command: app:generate-thumbnail 1-unsplash.jpg
    environment:
      APP_ENV: dev
      APP_DEBUG: 0
      SOURCE_IMAGE_DIR: /data/images
      BUCKET_CREDENTIALS_KEY: ${BUCKET_CREDENTIALS_KEY}
      BUCKET_CREDENTIALS_SECRET: ${BUCKET_CREDENTIALS_SECRET}
      BUCKET_REGION: ${BUCKET_REGION}
      BUCKET_SERVER_ENDPOINT: http://minio:9000

  nginx: # proxy for php-fpm, in production we have to avoid expose php instance, ingress should be controlled by proxy
    build:
      context: docker/nginx
      dockerfile: Dockerfile
    container_name: nginx
    restart: unless-stopped
    volumes:
      - ${APP_DIR}:/var/www/html:delegated
      - ./docker/nginx/conf.d/app.conf:/etc/nginx/conf.d/app.conf
      - ./docker/nginx/nginx.conf:/etc/nginx/nginx.conf
    environment:
      PHP_FPM_HOST: app
    depends_on:
      - app
    ports:
      - 80:80
      - 443:443

  minio: # simulate AWS S3 object storage (and any other of this kind)
    image: minio/minio # use specified tag, not latest, and cache mirrors in your image registry
    container_name: minio
    command: server /data --console-address ":9001"
    networks:
      default:
        aliases:
          - ${BUCKET_NAME}.minio #temporary host resolution for bucket in docker network
    ports:
      - 9000:9000
      - 9001:9001
    volumes:
      - minio_data:/data
    environment:
      - MINIO_ROOT_USER=${BUCKET_CREDENTIALS_KEY}
      - MINIO_ROOT_PASSWORD=${BUCKET_CREDENTIALS_SECRET}
      - MINIO_REGION=${BUCKET_REGION}
      - MINIO_DOMAIN=minio

  minio_client: # only for presentation view, just creating bucket with public access
    image: minio/mc # use specified tag, not latest, and cache mirrors in your image registry
    container_name: minio_client
    depends_on:
      - minio
    environment:
      - BUCKET_CREDENTIALS_KEY
      - BUCKET_CREDENTIALS_SECRET
      - BUCKET_REGION
      - BUCKET_NAME
    entrypoint: >
      /bin/sh -c "
      /usr/bin/mc config host add app minio:9000 $BUCKET_CREDENTIALS_KEY $BUCKET_CREDENTIALS_SECRET;
      /usr/bin/mc mb --region $BUCKET_REGION --ignore-existing app/${BUCKET_NAME}/;
      /usr/bin/mc policy set public app/${BUCKET_NAME}/;
      "

volumes:
  minio_data: