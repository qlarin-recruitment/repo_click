upstream backend {
    # The number of idle keepalive connections to an upstream server that remain open for each worker process
    server app:9000;
    keepalive 40;
    keepalive_requests 250; # Must be less than php-fpm.conf:pm.max_requests
    keepalive_timeout 10;
}

server {
    listen 80;
    listen [::]:80;
    server_name localhost;
    root /var/www/html/public;

    index index.php;

    # index.php fallback
    location / {
        try_files $uri /index.php$is_args$args;
    }

    location ~ ^/index.php(/|$) {
        # default fastcgi_params
        include fastcgi_params;

        # fastcgi settings
        fastcgi_pass backend;
        fastcgi_index index.php;
        fastcgi_buffers 8 16k;
        fastcgi_buffer_size 32k;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;

        # fastcgi params
        fastcgi_param DOCUMENT_ROOT $realpath_root;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;

        internal;
    }

    location ~ ^/images/favicon/  { access_log off; log_not_found off; }
    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }
    
    # return 404 for all other php files not matching the front controller
    location ~ \.php$ { return 404;}

    # deny all dot files except .well-known
    location ~ /\.(?!well-known) {
        deny all;
    }
}