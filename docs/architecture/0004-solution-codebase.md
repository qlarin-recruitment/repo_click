# 4. Solution Codebase

Date: 2022-04-22

## Status

Accepted

## Context

We need to decide how to manage our codebase.

## Decision

The application will be built as monorepo on a single repository.

## Consequences

For this project, there is no need to split the codebase into many repositories. On the contrary, we should start with monolith architecture and one source of truth. We focus on compose code to provide support with eventual transformation to a more scalable approach.

What we get in that approach:

* Centrally located code simplifies management and collaboration with other developers.
* There are no custom, multiple libraries to track and refactor. All modifications to all code will be submitted in one repository, under a single merge request. That provides an easier inspection process.
* We will minimize some unnecessary effects acquired from other libraries.
* Versioning will be easier.
* Multiple codebases, it's not an app - it's a distributed system. Even in future business needs, there are not enough reasons to start with many components.
