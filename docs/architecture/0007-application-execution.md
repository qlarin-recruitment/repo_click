# 7. Application Execution

Date: 2022-04-22

## Status

Accepted

## Context

We need to execute an application in an environment.

## Decision

The application will be executed as one or more stateless processes. Each instance will be runned independently and won't share nothing. Every data that needs to persist will be stored in a stateful backing service.

* The main task requires a CLI interface to execute thumbnails generation. That is a common scenario for an administrative process that should be executed as a one-off process.
* In the future, the same codebase will provide an API interface that will be deployed as a web application in different instances as a long-running process. Also stateless.

## Consequences

That approach provides strong elasticity in scale and distinguishes one-time tasks from long-running processes.

* We achieve space and the possibility to run tasks whenever we need but that would be available only for a limited group.
* If there will be a need to prepare some frontend application or API interface which integrates with business logic that would be no problem in the current model.
* For now we don't see any requirements to host long-running REPL environment. That would be only a waste of resources.
