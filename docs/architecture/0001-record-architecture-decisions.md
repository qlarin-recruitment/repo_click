# 1. Record architecture decisions

Date: 2022-04-22

## Status

Accepted

## Context

We need to record the architectural decisions made on this recruitment example project.

## Decision

We will use Lightweight Architecture Decision Records, as described by Michael Nygard in
[documenting architecture decisions](http://thinkrelevance.com/blog/2011/11/15/documenting-architecture-decisions).

## Consequences

See Michael Nygard's article, linked above.

* For now, we will focus on proper documentation of our architectural decisions to justify and simplify our understanding of the steps taken in building the solution.
* Secondly, ADR is widely adopted in the Software Development community. People know the context and structure of this kind of documentation.
* The lightweight version of ADR is more flexible so will be likely more readable.
