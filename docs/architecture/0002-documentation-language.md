# 2. Documentation Language

Date: 2022-04-22

## Status

Accepted

## Context

We need to choose language to properly document our code, architecture, and every other aspect of the solution.

## Decision

We will stay with english.

## Consequences

The English language is common, and what's the more, native and natural language for the software development process.

* We focused on english to shows our level of understanding.
* There is a chance to be assessed by a non-polish-native developer in the recruitment process. English is a safe choice.
