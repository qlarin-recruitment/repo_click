# 3. Local Platform

Date: 2022-04-22

## Status

Accepted

## Context

We need to decide upon a platform to host the local solution infrastructure. There are no long-term plans for that solution in the future but we should focus on portability and evade vendor lock-in.

## Decision

We are using docker with compose specifications for defining and running multi-container applications.

## Consequences

Containers work in all environments and are the easiest way to explain the idea of the target environment.

* For that kind of project, there is no need to create a dedicated Kubernetes cluster to orchestrate containers so we will stay with compose tool.
* There is a chance that the client does not know anything except Docker itself to run solutions in a portable environment so it is a natural choice for the recruitment project.
* It's no problem with running solutions in cloud platforms of any choice. Containers could be run even in some native cloud services. There are some practices and factors that we have to provide during development but it will not cost much extra time.
