# 5. Solution Configuration

Date: 2022-04-22

## Status

Accepted

## Context

We need to decide how to store configuration for our application.

## Decision

We require strict separation of config from code.

* As for config, we mean everything that is to vary between deployments for destination environments. This includes any integration data to backing services or external services such as Storage, Database, and so on.
* Internal application config for routes definition and other code that does not vary between deployments will be part of the application.

## Consequences

Environment-based configurations are easy to change between deployments without changing source code. What is more, there is an additional security benefit. We won't store secrets in our codebase and we achieve independently managed configuration for each deployment. That model will scale up smoothly.
