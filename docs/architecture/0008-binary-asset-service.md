# 8. Binary Asset Service

Date: 2022-04-22

## Status

Accepted

## Context

We need to choose how to attach binary resources data to the application.

## Decision

We will follow recruitment task requirements, but with some modifications.

* As a remote binary asset service, we will choose MinIO as an Amazon S3 replacement. There is no real difference but in a development environment will be a cheaper and faster choice. The integration interface is the same for both solutions.
* Local disk storage is not recommended because of the high coupling risk and limited reusability in different processes. That is only a common choice for cache, temporary data. We will attach stateful backing service such as persistent storage. In the future, we could replace it with some distributed filesystem. For now, is not necessary.

## Consequences

Attached backing services provide an opportunity to easy replacement and optimization in the future. There is room for performance, stability, and security testing in the integration process also.

No strict configuration and dependency in source code fully support this approach.

That approach gives us robustness and secures disposable application processes which scale properly in the concurrency model.
