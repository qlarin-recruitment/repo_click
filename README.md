# repo_click

Just another requitment task ;)

## Description

That is my proof of concept. There is more work to do in case stable, good quality, and performance solutions.

What should be done, for example:

- Comparison for thumbnail generation solutions. I choose Intervention Image but Symfony have promising bundle for that operations.
- Nginx configuration to look in healthcheck status of php-fpm. I have installed some bundle in app but this should be swapped with dedicated POSIX php-fpm-healthcheck.
- Add some logging for Event Driven Development.
- Install elastic + kibana (maybe).
- Integrate FluentD or another log collector to process logs and sent them to elastic or different destination.
- Integrate Prometheus or another metrics solution and scrape app and sidecars metrics.
- Install Grafana for typical dashboarding and integrate with Prometheus instance (or different data source).
- Refactor Dockerfiles to achieve multi stage specification. Production targets should be slim, without extra code and configuration.
- Process secrets with KMS, no configmaps.
- Switch to Kubernetes cluster instead of docker. Never use docker on production.
- In case of slim app we can little refactor and rearchitect to build cloud native app, for example in Google Cloud App Engine.
- Task is easy enough to build serverless solution. We should take it into account. Consider it.
- PHP is not my first choice. We could achieve better performance with Golang or Elixir stacks.
- New business needs require some refactoring. Always should be considered.
- Maybe some asynchronous processing? There is always a chance to face some network or machine problems.
- In case of future API, let's process trace routes with otel collector.

**And so on.. we can talk about it hours and hours. But what is really important and essential - to take small steps, just in time, just enough.**

## Architecture Decision Records

There are some sample ADR from me just for that occasion. Check it out :)

- [Record architecture decisions](docs/architecture/0001-record-architecture-decisions.md)
- [Documentation language](docs/architecture/0002-documentation-language.md)
- [Local platform](docs/architecture/0003-local-platform.md)
- [Solution codebase](docs/architecture/0004-solution-codebase.md)
- [Logs](docs/architecture/0006-logs.md)
- [Application execution](docs/architecture/0007-application-execution.md)
- [Binary asset service](docs/architecture/0008-binary-asset-service.md)

In real projects - should and even have to be more. I have skipped some writing to save time.

## Installation & Usage

There is no real installation. Just need docker installed on your workstation.

## Build and run composite environment with some automated tasks in background

```bash
docker compose up -d --build
```

## Log-in to php container as root (if you need)

```bash
docker exec -u 0 -it app bash
```

## Exec command in your terminal

```bash
docker exec -it app php bin/console app:generate-thumbnail 1-unsplash.jpg
```

## Authors and acknowledgment

[Stanisław Śledziona](https://gitlab.com/qlarin)

## License

For open source projects, just for recruitment process.
