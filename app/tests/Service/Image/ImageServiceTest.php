<?php

namespace App\Tests\Service\Image;

use PHPUnit\Framework\TestCase;
use App\Service\ImageService;
use App\Service\Image\ServicePlanBasic;
use App\Service\Image\ServicePlanInterface;

class ImageServiceTest extends TestCase
{
    public function testSetServicePlanBasic(): void
    {
        $imageService = new ImageService(ImageService::BASIC_PLAN);
        $servicePlan = $imageService->getServicePlan();

        $this->assertInstanceOf(ServicePlanBasic::class, $servicePlan);
        $this->assertInstanceOf(ServicePlanInterface::class, $servicePlan);
    }
}
