<?php

namespace App\Tests\Command;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class GenerateThumbnailCommandTest extends KernelTestCase
{
    public function testExecute(): void
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);

        $command = $application->find('app:generate-thumbnail');
        $sourceDirectory = $command->getSourceDirectory();
        $sourceImageFile = '1-unsplash.jpg';

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'file' => $sourceImageFile,
            '--dest_dir' => 'thumbnails',
        ]);

        $output = $commandTester->getDisplay();
        $this->assertDirectoryExists($sourceDirectory);
        $this->assertDirectoryIsReadable($sourceDirectory);
        $this->assertFileExists($sourceDirectory . '/' . $sourceImageFile);
        $this->assertStringContainsString('Destination directory: thumbnails', $output);
        $this->assertStringContainsString('Image thumbnail: ' . $sourceImageFile . ' has been successfully uploaded.', $output);
    }
}
