<?php

namespace App\Service;

use App\Service\Image\ServicePlanBasic;
use App\Service\Image\ServicePlanInterface;

class ImageService
{
    public const BASIC_PLAN = 'basic';

    private ServicePlanInterface $servicePlan;

    public function __construct(string $servicePlan = self::BASIC_PLAN)
    {
        $this->setServicePlan($servicePlan);
    }

    /**
     * Create thumbnail for requested image
     *
     * @param mixed $file source of processed file
     * @param string $destination destination of saved image
     * @return boolean thumbnail successfully created?
     */
    public function createThumbnail($file, $destination)
    {
        return $this->getServicePlan()->loadImage($file)->resize()->saveImage($destination);
    }

    /**
     * Return active service plan
     *
     * @return ServicePlanInterface service plan instance
     */
    public function getServicePlan(): ServicePlanInterface
    {
        return $this->servicePlan;
    }

    /**
     * Change service plan
     *
     * @param string $servicePlan service plan name
     */
    private function setServicePlan(string $servicePlan): void
    {
        if ($servicePlan == self::BASIC_PLAN) {
            $this->servicePlan = new ServicePlanBasic();
        } else {
            throw new Exception("Unrecognized image service plan");
        }
    }
}
