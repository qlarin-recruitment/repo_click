<?php

namespace App\Service\Image;

interface ServicePlanInterface
{
    public function loadImage($file);

    public function saveImage(string $destination);

    public function resize(int $width, int $height);
}
