<?php

namespace App\Service\Image;

use Intervention\Image\Image;
use Intervention\Image\ImageManager;
use Aws\S3\S3Client;

class ServicePlanBasic extends AbstractServicePlan
{
    protected const SIZE_LIMIT = 150;
    protected const DEFAULT_SAVE_STRATEGY = 'S3Client';
    protected const DEFAULT_SAVE_FORMAT = 'jpg';
    protected const DEFAULT_SAVE_QUALITY = '90';

    private Image $image;
    private ImageManager $imageManager;
    private S3Client $s3Client;

    public function __construct(string $imageDriver = 'imagick')
    {
        $this->imageManager = new ImageManager(['driver' => $imageDriver]);
        $this->s3Client = $this->getS3Client();

        parent::__construct();
    }

    /**
     * @inheritdoc
     */
    public function loadImage($file): ServicePlanInterface
    {
        $this->image = $this->imageManager->make($file);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function saveImage(string $destination, string $strategy = self::DEFAULT_SAVE_STRATEGY): bool
    {
        $this->image->save('/tmp/' . $this->image->basename, self::DEFAULT_SAVE_QUALITY, self::DEFAULT_SAVE_FORMAT);

        return $this->{'saveWith' . $strategy}($destination);
    }

    /**
     * @inheritdoc
     */
    public function resize(int $width = self::SIZE_LIMIT, int $height = self::SIZE_LIMIT): ServicePlanInterface
    {
        $requestedWidth = $this->calculateWidth($width);
        $requestedHeight = $this->calculateHeight($height);

        // resize the image so that the largest side fits within the limit; the smaller
        // side will be scaled to maintain the original aspect ratio
        $this->image->resize($requestedWidth, $requestedHeight, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        return $this;
    }

    /**
     * @inheritdoc
     */
    protected function calculateWidth(?int $width): int
    {
        return ($width <= self::SIZE_LIMIT) ? $width : self::SIZE_LIMIT;
    }

    /**
     * @inheritdoc
     */
    protected function calculateHeight(?int $height): int
    {
        return ($height <= self::SIZE_LIMIT) ? $height : self::SIZE_LIMIT;
    }

    /**
     * Save image in requested destination bucket
     *
     * @param string $destination destination bucket name
     * @return bool $result image successfully saved?
     */
    protected function saveWithS3Client(string $destination): bool
    {
        try {
            $basename = $this->image->basename;
            $this->s3Client->putObject([
                'Bucket' => $destination,
                'Key'    => $basename,
                'Body' => fopen('/tmp/' . $basename, 'r'),
            ]);
        } catch (Aws\S3\Exception\S3Exception $e) {
            echo "There was an error uploading the file.\n";
            return false;
        }

        return true;
    }

    /**
     * Return new storage s3Client instance
     *
     * @return S3Client storage client instance
     */
    private function getS3Client(): S3Client
    {
        return new s3Client([
            'version' => 'latest',
            'region' => getenv('BUCKET_REGION'),
            'endpoint' => getenv('BUCKET_SERVER_ENDPOINT'),
            'credentials' => [
                'key' => getenv('BUCKET_CREDENTIALS_KEY'),
                'secret' => getenv('BUCKET_CREDENTIALS_SECRET'),
            ],
        ]);
    }
}
