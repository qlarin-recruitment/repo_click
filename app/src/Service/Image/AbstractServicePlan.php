<?php

namespace App\Service\Image;

abstract class AbstractServicePlan implements ServicePlanInterface
{
    public function __construct()
    {
    }

    /**
     * Load image to service plan instance
     *
     * @param mixed $file source of processed file
     * @return ServicePlanInterface service plan instance
     */
    abstract public function loadImage($file): ServicePlanInterface;

    /**
     * Save image in requested destination
     *
     * @param string $destination destination of saved image
     * @return bool image successfully uploaded?
     */
    abstract public function saveImage(string $destination): bool;

    /**
     * Resize requested image file with possible constraints
     *
     * @param int $width
     * @param int $height
     * @return ServicePlanInterface service plan instance
     */
    abstract public function resize(int $width, int $height): ServicePlanInterface;

    /**
     * Return calculated width
     *
     * @param ?int $width requested width
     * @return int width
     */
    abstract protected function calculateWidth(?int $width): int;

    /**
     * Return calculated height
     *
     * @param ?int $height requested height
     * @return int height
     */
    abstract protected function calculateHeight(?int $height): int;
}
