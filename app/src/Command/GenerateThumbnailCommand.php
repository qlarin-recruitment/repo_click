<?php

namespace App\Command;

use App\Service\ImageService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateThumbnailCommand extends Command
{
    public const SUCCESS = 0;
    public const FAILURE = 1;
    public const INVALID = 2;

    protected static $defaultName = 'app:generate-thumbnail';
    protected static $defaultSourceDirectory = '/data/images';

    private string $sourceDirectory;

    public function __construct(string $sourceImageDir, string $name = null)
    {
        $this->sourceDirectory = $sourceImageDir;

        parent::__construct($name);
    }

    /**
     * Return current image source directory
     *
     * @return string source directory
     */
    public function getSourceDirectory(): string
    {
        return $this->sourceDirectory ?: self::$defaultSourceDirectory;
    }

    /**
     * @inheritdoc
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Generates a new thumbnail from requested image file.')
            ->setHelp('This command allows you to generate a new thumbnail for requested image file and save it on remote object storage...')
            ->addArgument('file', InputArgument::REQUIRED, 'Image file')
            ->addOption('dest_dir', 'dd', InputOption::VALUE_OPTIONAL, 'Destination directory', 'thumbnails')
            ->addOption('service_plan', 'sp', InputOption::VALUE_OPTIONAL, 'Service plan', ImageService::BASIC_PLAN);
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln(['Generating thumbnail', '====================', 'Please wait...']);

        $file = $input->getArgument('file');
        $destinationDirectory = $input->getOption('dest_dir');

        $imageService = new ImageService($input->getOption('service_plan'));
        $state = $imageService->createThumbnail($this->sourceDirectory . '/' . $file, $destinationDirectory);

        if ($state) {
            $output->writeln('Destination directory: ' . $destinationDirectory);
            $output->writeln('Image thumbnail: ' . $input->getArgument('file') . ' has been successfully uploaded.');

            return self::SUCCESS;
        }

        return self::FAILURE;
    }
}
