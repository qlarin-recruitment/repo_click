# Recruitment Task

Attention! To save some time, I will let it be as is, with no translation to English.

**Zakładamy, że znamy minimalne kryteria biznesowe dla wdrożenia serwisu:**

* Wygenerowanie miniatury przy użyciu komendy CLI
* Plik źródłowy znajduje się na dysku lokalnym
* Plik docelowy zostaje zapisany w buckecie S3 AWS
* Miniaturka ma stałe wymiary - dłuższy bok to maksymalnie 150px

**Wiemy, że Product Owner w nieokreślonej przyszłości będzie chciał znacznie rozszerzać funkcjonalność i obszar działania serwisu. Nie znamy planów, ale pewne przesłanki sugerują nam, że od strony biznesu największym zainteresowaniem cieszą się następujące kierunki rozwoju:**

* Przyjmowanie danych wejściowych poprzez API równolegle do procesów CLI
* Alternatywne rozwiązania chmurowe na zapis docelowego pliku
* Przycinanie obrazka
* Dodawanie znaku wodnego
* Stworzenie miniatury premium dostępnej tylko dla niektórych klientów (np o innym rozmiarze dłuższego boku)

Mając na uwadze powyższe przesłanki, jak również nieprzewidywalność biznesu przygotuj aplikację w oparciu o dowolnie wybrany framework.

Najistotniejszym elementem zadania jest `dobór architektury` i `przykładowych technologii`. Dopuszczalne jest zaprezentowanie pewnych obszarów poprzez opis słowny lub pseudokod, jednocześnie nie zapominaj o jakości pozostałego kodu i jego zgodności ze standardami.
